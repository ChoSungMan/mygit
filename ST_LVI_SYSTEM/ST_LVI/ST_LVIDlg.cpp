
// ST_LVIDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "ST_LVI.h"
#include "ST_LVIDlg.h"
#include "afxdialogex.h"
#include "blobDetector.h"
#include "preProcessor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CST_LVIDlg 대화 상자

areaDetector _areaDetector;
preProcessor _preProcessor;
characterDetector _characterDetector;

CST_LVIDlg::CST_LVIDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CST_LVIDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CST_LVIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MAT_PICTURE, m_pic);
	DDX_Control(pDX, IDC_BINARY_STATIC, m_pImage);
}

BEGIN_MESSAGE_MAP(CST_LVIDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_COMMAND(ID_FILE_LOAD, &CST_LVIDlg::OnFileLoad)
	ON_COMMAND(ID_FIND_CHARACTERREGION, &CST_LVIDlg::OnFindArea)
	ON_COMMAND(ID_FIND_CHARATER, &CST_LVIDlg::OnFindCharater)
	ON_COMMAND(ID_FILE_EXIT, &CST_LVIDlg::OnFileExit)
	ON_COMMAND(ID_OPTIONS_CLEAR, &CST_LVIDlg::OnOptionsClear)
END_MESSAGE_MAP()


// CST_LVIDlg 메시지 처리기

BOOL CST_LVIDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	this->SetWindowPos(NULL, 100, 100, 1460, 820, SWP_NOREPOSITION);
	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	// Set Tool's Position
	CWnd *pWnd1 = (CWnd*)GetDlgItem(IDC_MAT_PICTURE);
	pWnd1->MoveWindow(40, 40, 640, 480);

	CWnd *pWnd2 = (CWnd*)GetDlgItem(IDC_BINARY_STATIC);
	pWnd2->MoveWindow(720, 40, 640, 480);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CST_LVIDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CST_LVIDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CST_LVIDlg::OnFileLoad()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	CFileDialog dlg(TRUE, _T("*.jpg"), NULL, OFN_FILEMUSTEXIST, _T("JPG Files(*.jpg)|*.jpg|"), NULL);

	if (dlg.DoModal() == IDOK)
	{
		c_strPathName = dlg.GetPathName();
		string s_strPath = CT2CA(c_strPathName.operator LPCWSTR());
		src = imread(s_strPath);
		result = src.clone();

		binary = _preProcessor.makeBinary(src,100);

		DisplayImage(result);
		DisplayBinaryImage(binary);
	}
}

void CST_LVIDlg::DisplayImage(Mat &_targetMat)
{
	CDC *pDC;
	CImage *mfcImg = nullptr;

	pDC = m_pic.GetDC();

	Mat temp;
	resize(_targetMat, temp, Size(640, 480));

	BITMAPINFO bitmapInfo;
	bitmapInfo.bmiHeader.biYPelsPerMeter = 0;
	bitmapInfo.bmiHeader.biBitCount = 24;
	bitmapInfo.bmiHeader.biWidth = temp.cols;
	bitmapInfo.bmiHeader.biHeight = temp.rows;
	bitmapInfo.bmiHeader.biPlanes = 1;
	bitmapInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitmapInfo.bmiHeader.biCompression = BI_RGB;
	bitmapInfo.bmiHeader.biClrImportant = 0;
	bitmapInfo.bmiHeader.biClrUsed = 0;
	bitmapInfo.bmiHeader.biSizeImage = 0;
	bitmapInfo.bmiHeader.biXPelsPerMeter = 0;

	if (_targetMat.channels() == 3)
	{
		mfcImg = new CImage();
		mfcImg->Create(temp.cols, temp.rows, 24);
	}

	else if (_targetMat.channels() == 1)
	{
		cvtColor(temp, temp, CV_GRAY2RGB);
		mfcImg = new CImage();
		mfcImg->Create(temp.cols, temp.rows, 24);
	}

	else if (_targetMat.channels() == 4)
	{
		bitmapInfo.bmiHeader.biBitCount = 32;
		mfcImg = new CImage();
		mfcImg->Create(temp.cols, temp.rows, 32);
	}

	flip(temp, temp, 0);

	::StretchDIBits(mfcImg->GetDC(), 0, 0, temp.cols, temp.rows, 0, 0, temp.cols, temp.rows, temp.data, &bitmapInfo,
		DIB_RGB_COLORS, SRCCOPY);

	mfcImg->BitBlt(::GetDC(m_pic.m_hWnd), 0, 0);

	if (mfcImg)
	{
		mfcImg->ReleaseDC();
		delete mfcImg; mfcImg = nullptr;
	}
	temp.release();
	ReleaseDC(pDC);
}

void CST_LVIDlg::DisplayBinaryImage(Mat &_targetMat)
{
	CDC *pDC;
	CImage *mfcImg = nullptr;

	pDC = m_pImage.GetDC();

	Mat temp;
	resize(_targetMat, temp, Size(640, 480));

	BITMAPINFO bitmapInfo;
	bitmapInfo.bmiHeader.biYPelsPerMeter = 0;
	bitmapInfo.bmiHeader.biBitCount = 24;
	bitmapInfo.bmiHeader.biWidth = temp.cols;
	bitmapInfo.bmiHeader.biHeight = temp.rows;
	bitmapInfo.bmiHeader.biPlanes = 1;
	bitmapInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitmapInfo.bmiHeader.biCompression = BI_RGB;
	bitmapInfo.bmiHeader.biClrImportant = 0;
	bitmapInfo.bmiHeader.biClrUsed = 0;
	bitmapInfo.bmiHeader.biSizeImage = 0;
	bitmapInfo.bmiHeader.biXPelsPerMeter = 0;

	if (_targetMat.channels() == 3)
	{
		mfcImg = new CImage();
		mfcImg->Create(temp.cols, temp.rows, 24);
	}

	else if (_targetMat.channels() == 1)
	{
		cvtColor(temp, temp, CV_GRAY2RGB);
		mfcImg = new CImage();
		mfcImg->Create(temp.cols, temp.rows, 24);
	}

	else if (_targetMat.channels() == 4)
	{
		bitmapInfo.bmiHeader.biBitCount = 32;
		mfcImg = new CImage();
		mfcImg->Create(temp.cols, temp.rows, 32);
	}

	flip(temp, temp, 0);

	::StretchDIBits(mfcImg->GetDC(), 0, 0, temp.cols, temp.rows, 0, 0, temp.cols, temp.rows, temp.data, &bitmapInfo,
		DIB_RGB_COLORS, SRCCOPY);

	mfcImg->BitBlt(::GetDC(m_pImage.m_hWnd), 0, 0);

	if (mfcImg)
	{
		mfcImg->ReleaseDC();
		delete mfcImg; mfcImg = nullptr;
	}
	temp.release();
	ReleaseDC(pDC);
}

void CST_LVIDlg::OnFindArea()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.	
	_areaDetector.findBlob(binary, binaryResult, regionRect);

	for (int i = 0; i < regionRect.size(); i++)
	{
		rectangle(result, regionRect[i], Scalar(255, 0, 0), 2, 8);
		putText(result, to_string(i), Point(regionRect[i].x, regionRect[i].y - 4), 1, 1, Scalar(0, 0, 255), 2);
	}

	DisplayImage(result);
	DisplayBinaryImage(binaryResult);
}


void CST_LVIDlg::OnFindCharater()
{
	_characterDetector.findCharacter(binary, binaryResult, characterRect);

	for (int i = 0; i < characterRect.size(); i++)
	{
		rectangle(result, characterRect[i], Scalar(0, 0, 255), 2, 8);
	}

	DisplayImage(result);
	DisplayBinaryImage(binaryResult);
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
}


void CST_LVIDlg::OnFileExit()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	exit(0);
}


void CST_LVIDlg::OnOptionsClear()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	result = src.clone();
	binaryResult = binary.clone();

	DisplayImage(result);
	DisplayBinaryImage(binaryResult);
}