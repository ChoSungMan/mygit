
// ST_LVIDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"


// CST_LVIDlg 대화 상자
class CST_LVIDlg : public CDialogEx
{
// 생성입니다.
public:
	CST_LVIDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ST_LVI_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;
	CString c_strPathName;

	// opencv
	Mat src;
	Mat result;
	Mat binary;
	Mat binaryResult;

	vector<Rect> regionRect;
	vector<Rect> characterRect;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	CStatic m_pic;
	afx_msg void OnFileLoad();
	afx_msg void OnBnClickedButton1();
	void DisplayImage(Mat &_t);
	afx_msg void OnFindArea();
	afx_msg void OnFindCharater();
	afx_msg void OnFileExit();
	afx_msg void OnOptionsClear();
	CStatic m_pImage;
	void DisplayBinaryImage(Mat &_t);
};
