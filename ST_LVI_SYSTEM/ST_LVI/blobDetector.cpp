#include "stdafx.h"
#include "blobDetector.h"

areaDetector::areaDetector()
{

}

areaDetector::~areaDetector()
{

}

void areaDetector::findBlob(Mat input, Mat &output, vector<Rect> &region, int kernelparam_h, int kernelparam_w)
{
	Mat adMat = input.clone();
	// morphology
	Mat kernel(kernelparam_w, kernelparam_h, CV_8UC1, Scalar(1));
	morphologyEx(adMat, adMat, MORPH_CLOSE, kernel);

	output = adMat.clone();

	// find contour
	vector<vector<Point>>contours;
	findContours(adMat, contours, RETR_EXTERNAL, CHAIN_APPROX_NONE);
	
	// find & draw rect
	for (int i = 0; i < contours.size(); i++)
	{		
		region.push_back(boundingRect(contours[i]));
	}
}

// character Detector //

characterDetector::characterDetector()
{

}

characterDetector::~characterDetector()
{

}

void characterDetector::findCharacter(Mat input, Mat &output, vector<Rect> &region,int kernelparam_h, int kernelparam_w)
{
	Mat cdMat = input.clone();

	// morphology
	Mat kernel(kernelparam_w, kernelparam_h, CV_8UC1, Scalar(1));
	morphologyEx(cdMat, cdMat, MORPH_CLOSE, kernel);

	output = cdMat.clone();

	// find contour
	vector<vector<Point>>contours;
	findContours(cdMat, contours, RETR_EXTERNAL, CHAIN_APPROX_NONE);

	// find & draw rect
	for (int i = 0; i < contours.size(); i++)
	{
		region.push_back(boundingRect(contours[i]));
	}
}