#pragma once

class areaDetector
{

private:
	
	
public:
	areaDetector();
	~areaDetector();

	void findBlob(Mat input, Mat &output, vector<Rect> &region, int kernelparam_w = 40, int kernelparam_h = 5);
};

class characterDetector
{

private:

public:

	characterDetector();
	~characterDetector();

	void findCharacter(Mat input, Mat &output, vector<Rect> &region, int kernelparam_w=3, int kernelparam_h=3);
};