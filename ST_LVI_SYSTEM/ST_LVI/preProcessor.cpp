#include "stdafx.h"
#include "preProcessor.h"

preProcessor::preProcessor()
{

}

preProcessor::~preProcessor()
{

}

Mat preProcessor::makeBinary(Mat s, int thresh)
{
	Mat bMat = s.clone();
	cvtColor(bMat, bMat, CV_RGB2GRAY);
	threshold(bMat, bMat, thresh, 255, CV_THRESH_BINARY_INV);

	return bMat;
}
