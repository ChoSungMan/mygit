//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// ST_LVI.rc에서 사용되고 있습니다.
//
#define IDD_ST_LVI_DIALOG               102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDC_MAT_PICTURE                 1001
#define IDC_BINARY_STATIC               1002
#define ID_FILE_LOAD                    32771
#define ID_FILE_EXIT                    32772
#define ID_FILE_SAVE32773               32773
#define ID_OPTIONS_CONFIGURATION        32774
#define ID_OPTIONS_TEACHING             32775
#define ID_FIND_CHARACTERREGION         32776
#define ID_FIND_CHARATER                32777
#define ID_OPTIONS_CLEAR                32778

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
